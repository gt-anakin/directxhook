// RAMhijack.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//
#include <easyhook.h>
#include <Windows.h>
#include <iostream>
#include <TlHelp32.h>
#include <string>

BOOL WINAPI myHook(DWORD dwFreq, DWORD dwDuration)
{
	std::cout << "Hooked!!\n";
	return Beep(dwFreq + 800, dwDuration);
}

DWORD getProcessID(const wchar_t *name)
{
	DWORD foundID = NULL;

	std::wcout << name << std::endl;

	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE)
		{
			if (wcscmp(name, entry.szExeFile) == 0)
			{
				foundID = entry.th32ProcessID;
				std::wstring ws(entry.szExeFile);
				std::string str(ws.begin(), ws.end());
				std::cout << entry.th32ProcessID << " - " << str << "\n";
				break;
			}
			//std::wstring ws(entry.szExeFile);
			//// your new String
			//std::string str(ws.begin(), ws.end());
			//std::cout << entry.th32ProcessID << " - " << str << "\n";
		}
	}

	CloseHandle(snapshot);

	return foundID;
}

int main()
{
	HOOK_TRACE_INFO hHook = { NULL };

	NTSTATUS result = LhInstallHook(GetProcAddress(GetModuleHandle(TEXT("kernel32")), "Beep"), myHook, NULL, &hHook);
	
	if (FAILED(result))
	{
		// Hook could not be installed, see RtlGetLastErrorString() for details
		return 0;
	}

	DWORD test = getProcessID(L"Target.exe");

	ULONG ACLEntries[1] = { test };


	std::cout << "I got this: " << test << std::endl;
	std::cin.get();


	// Enable the hook for the provided threadIds
	LhSetInclusiveACL(ACLEntries, 1, &hHook);

	std::cout << "Hook active. Press any key to disable.\n";
	std::cin.get();

	// Remove the hook handler
	LhUninstallHook(&hHook);
	LhWaitForPendingRemovals();

	std::cout << "Hook uninstalled.\n";
}

// Programm ausführen: STRG+F5 oder "Debuggen" > Menü "Ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
