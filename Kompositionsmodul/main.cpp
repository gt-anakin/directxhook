#ifndef UNICODE
#define UNICODE
#endif

#include <windows.h>
#pragma comment(lib, "user32.lib")

#include <wrl.h>
using namespace Microsoft::WRL;
#include <dxgi1_3.h>
#include <d3d11_2.h>
#include <d2d1_2.h>
#include <d2d1_2helper.h>
#include <dcomp.h>
#pragma comment(lib, "dxgi")
#pragma comment(lib, "d3d11")
#pragma comment(lib, "d2d1")
#pragma comment(lib, "dcomp")

struct ComException
{
	HRESULT result;
	ComException(HRESULT const value) : result(value) {}
};

void HR(HRESULT const result)
{
	if (S_OK != result)
	{
		throw ComException(result);
	}
}

int __stdcall wWinMain(HINSTANCE module, HINSTANCE, PWSTR, int)
{
	WNDCLASS wc = {};
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hInstance = module;
	wc.lpszClassName = L"window";
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = [](HWND window, UINT message, WPARAM wparam, LPARAM lparam) -> LRESULT { if (WM_DESTROY == message) { PostQuitMessage(0); return 0; } return DefWindowProc(window, message, wparam, lparam); };
	RegisterClass(&wc);

	HWND const window = CreateWindowEx(WS_EX_NOREDIRECTIONBITMAP, wc.lpszClassName, L"Sample", WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, nullptr, nullptr, module, nullptr);
	
	ComPtr<ID3D11Device> direct3dDevice;

	HR(D3D11CreateDevice(nullptr,    // Adapter
						D3D_DRIVER_TYPE_HARDWARE,
						nullptr,    // Module
						D3D11_CREATE_DEVICE_BGRA_SUPPORT,
						nullptr,
						0, // Highest available feature level
						D3D11_SDK_VERSION,
						&direct3dDevice,
						nullptr,    // Actual feature level
						nullptr)
		);  // Device context

	ComPtr<IDXGIDevice> dxgiDevice;
	HR(direct3dDevice.As(&dxgiDevice));

	ComPtr<IDXGIFactory2> dxFactory;
	HR(CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, __uuidof(dxFactory), reinterpret_cast<void **>(dxFactory.GetAddressOf())));

	DXGI_SWAP_CHAIN_DESC1 description = {};
	description.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	description.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	description.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	description.BufferCount = 2;
	description.SampleDesc.Count = 1;
	description.AlphaMode = DXGI_ALPHA_MODE_PREMULTIPLIED;

	RECT rect = {};
	GetClientRect(window, &rect);
	description.Width = rect.right - rect.left;
	description.Height = rect.bottom - rect.top;

	ComPtr<IDXGISwapChain1> swapChain;
	HR(dxFactory->CreateSwapChainForComposition(dxgiDevice.Get(), &description, nullptr, // Don�t restrict
		swapChain.GetAddressOf()));

	// Direct2D-Factory mit Singlethread mit Debuginformationen erstellen
	ComPtr<ID2D1Factory2> d2Factory;
	D2D1_FACTORY_OPTIONS const options = { D2D1_DEBUG_LEVEL_INFORMATION };
	HR(D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, options, d2Factory.GetAddressOf())); // Das Direct2D-Ger�t erstellen, das mit dem Direct3D-Ger�t verkn�pft ist
	ComPtr<ID2D1Device1> d2Device; HR(d2Factory->CreateDevice(dxgiDevice.Get(), d2Device.GetAddressOf())); // Den Direct2D-Ger�tekontext erstellen, der das eigentliche Renderziel ist  // und die Zeichenbefehle aufdecken
	ComPtr<ID2D1DeviceContext> dc; HR(d2Device->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, dc.GetAddressOf())); // Den Hintergrundpuffer der Swapchain abrufen
	ComPtr<IDXGISurface2> surface; HR(swapChain->GetBuffer( 0, // index
		__uuidof(surface), reinterpret_cast<void **>(surface.GetAddressOf()))); // Ein Direct2D-Bitmap erstellen, das auf die Swapchain-Oberfl�che zeigt
	D2D1_BITMAP_PROPERTIES1 properties = {};
	properties.pixelFormat.alphaMode = D2D1_ALPHA_MODE_PREMULTIPLIED;
	properties.pixelFormat.format    = DXGI_FORMAT_B8G8R8A8_UNORM;
	properties.bitmapOptions         = D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW;
	ComPtr<ID2D1Bitmap1> bitmap;
	HR(dc->CreateBitmapFromDxgiSurface(surface.Get(), properties, bitmap.GetAddressOf())); // Den Ger�tekontext auf die Bitmap f�r das Rendering verweisen
	dc->SetTarget(bitmap.Get()); // Etwas zeichnen
	dc->BeginDraw();
	dc->Clear();
	ComPtr<ID2D1SolidColorBrush> brush;
	D2D1_COLOR_F const brushColor = D2D1::ColorF(0.18f,  // red
		0.55f,  // green
		0.34f,  // blue
		0.75f); // alpha
	HR(dc->CreateSolidColorBrush(brushColor, brush.GetAddressOf()));
	D2D1_POINT_2F const ellipseCenter = D2D1::Point2F(150.0f,  // x
		150.0f); // y
	D2D1_ELLIPSE const ellipse = D2D1::Ellipse(ellipseCenter, 100.0f,  // x radius
		100.0f); // y radius
	dc->FillEllipse(ellipse, brush.Get());
	HR(dc->EndDraw()); // Die Swapchain f�r das Kompositionsmodul verf�gbar machen
	HR(swapChain->Present(1,   // sync
		0)); // flags

	MSG message;
	
	while (BOOL result = GetMessage(&message, 0, 0, 0))
	{
		if (-1 != result)
			DispatchMessage(&message);
	}
}
