// Injector.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <iostream>
#include <string>
#include <cstring>
#include <easyhook.h>
#include <TlHelp32.h>


DWORD getProcessID(const wchar_t *name)
{
	DWORD foundID = NULL;

	std::wcout << name << std::endl;

	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE)
		{
			if (wcscmp(name, entry.szExeFile) == 0)
			{
				foundID = entry.th32ProcessID;
				std::wstring ws(entry.szExeFile);
				std::string str(ws.begin(), ws.end());
				std::cout << entry.th32ProcessID << " - " << str << "\n";
				break;
			}
			//std::wstring ws(entry.szExeFile);
			//// your new String
			//std::string str(ws.begin(), ws.end());
			//std::cout << entry.th32ProcessID << " - " << str << "\n";
		}
	}

	CloseHandle(snapshot);

	return foundID;
}

int main()
{
	DWORD processID = getProcessID(L"Target.exe");
	/*std::cout << "Enter the target process ID: ";
	std::cin >> processID;
	std::cout << "Compare with " << getProcessID(L"Target.exe");*/

	DWORD freqOffset = 500;
	//std::cout << "Enter a frequency offset in hertz: ";
	//std::cin >> freqOffset;
	
	WCHAR dllToInject[] = L"..\\Debug\\BeepHook.dll";
	//wprintf(L"Attempting to inject: %s\n\n", dllToInject);

	// Inject dll into the target process id, passing freqOffset as the pass through data
	NTSTATUS nt = RhInjectLibrary(processID, 0, EASYHOOK_INJECT_DEFAULT, dllToInject, NULL, &freqOffset, sizeof(DWORD));

	if (nt != 0)
	{
		printf("RhInjectLibrary failed with error code = %d\n", nt);
		PWCHAR err = RtlGetLastErrorString();
		std::wcout << err << "\n";

	}
	else
	{
		//std::wcout << L"Library injected successfully.\n";
	}

	/*std::wcout << "Press Enter to exit";
	std::wstring input;
	std::getline(std::wcin, input);
	std::getline(std::wcin, input);*/

	return 0;
}

// Programm ausführen: STRG+F5 oder "Debuggen" > Menü "Ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
